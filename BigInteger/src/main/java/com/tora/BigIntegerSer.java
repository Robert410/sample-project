package com.tora;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.ByteBuffer;

public class BigIntegerSer {
    public static final int kDefaultScale = 20;

    private static final byte[] NULL_INDICATOR = new BigInteger("-170141183460469231731687303715884105728")
            .toByteArray();

    private static final BigInteger scaleFactors[] = new BigInteger[] { BigInteger.ONE, BigInteger.TEN,
            BigInteger.TEN.pow(2), BigInteger.TEN.pow(3), BigInteger.TEN.pow(4), BigInteger.TEN.pow(5),
            BigInteger.TEN.pow(6), BigInteger.TEN.pow(7), BigInteger.TEN.pow(8), BigInteger.TEN.pow(9),
            BigInteger.TEN.pow(10), BigInteger.TEN.pow(11), BigInteger.TEN.pow(12) };

    static public void serializeBigDecimal(BigDecimal bd, ByteBuffer buf) {
        if (bd == null) {
            serializeNull(buf);
            return;
        }
        final int scale = bd.scale();
        final int precision = bd.precision();
        if (scale > 12) {
            throw new RuntimeException("Scale of " + bd + " is " + scale + " and the max is 12");
        }
        final int precisionMinusScale = precision - scale;
        if (precisionMinusScale > 26) {
            throw new RuntimeException("Precision of " + bd + " to the left of the decimal point is "
                    + precisionMinusScale + " and the max is 26");
        }
        final int scaleFactor = kDefaultScale - bd.scale();
        BigInteger unscaledBI = bd.unscaledValue().multiply(scaleFactors[scaleFactor]);
        boolean isNegative = false;
        if (unscaledBI.signum() < 0) {
            isNegative = true;
        }
        final byte unscaledValue[] = unscaledBI.toByteArray();
        if (unscaledValue.length > 16) {
            throw new RuntimeException("Precision of " + bd + " is >38 digits");
        }
        buf.put(expandToLength16(unscaledValue, isNegative));
        System.out.println(buf);
    }

    static public void serializeNull(ByteBuffer buf) {
        buf.put(NULL_INDICATOR);
    }

    private static final byte[] expandToLength16(byte scaledValue[], final boolean isNegative) {
        if (scaledValue.length == 16) {
            return scaledValue;
        }
        byte replacement[] = new byte[16];
        if (isNegative) {
            java.util.Arrays.fill(replacement, (byte) -1);
        }
        for (int ii = 15; 15 - ii < scaledValue.length; ii--) {
            replacement[ii] = scaledValue[ii - (replacement.length - scaledValue.length)];
        }
        return replacement;
    }
}

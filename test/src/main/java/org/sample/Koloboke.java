package org.sample;

import com.koloboke.collect.map.hash.HashIntIntMaps;

import java.util.Map;

public class Koloboke {
    Map<Integer,Integer> elements;

    public Koloboke(){
        elements  = HashIntIntMaps.newMutableMap();
    }

    public int add(int nr1, int nr2){
        return elements.put(nr1, nr2);
    }

    public boolean contains(int nr1){
        return elements.containsKey(nr1);
    }

    public boolean remove(int nr1, int nr2){
        return elements.remove(nr1,nr2);
    }
}

package org.sample;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrayListImpl<T> {
    private ArrayList<T> elements;

    public ArrayListImpl() {
        elements = new ArrayList<>();
    }

    public boolean add(T e) {
        return elements.add(e);
    }

    public T remove(int i) {
        return elements.remove(i);
    }

    public boolean contains(T e) {
        return elements.contains(e);
    }
}

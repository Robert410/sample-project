/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.sample;

import org.openjdk.jmh.annotations.Benchmark;

public class MyBenchmark {
    HashSetIplm<Object> elementsHashSet = new HashSetIplm<>();
    ArrayListImpl<Object> elementsArrayList = new ArrayListImpl<>();
    ConcurrentHashMapImpl<Object,Object> elementsConcurrentHash = new ConcurrentHashMapImpl<>();
    EclipseCollection<Object> elementsCollectionEclipse = new EclipseCollection<>();
    Koloboke elementsKoloboke = new Koloboke();
    TreeSetImpl<Object> elementsTreeSet = new TreeSetImpl<>();
    @Benchmark
    public void testHashSetAdd() {
        elementsHashSet.add(1);
        elementsHashSet.add(3);
    }

    @Benchmark
    public void testArrayListAdd(){
        elementsArrayList.add(1);
        elementsArrayList.add(3);
    }

    @Benchmark
    public void testConcurrentHashAdd(){
        elementsConcurrentHash.add(1,1);
        elementsConcurrentHash.add(3,1);
    }

    @Benchmark
    public void testEclipseAdd(){
        elementsCollectionEclipse.add(1);
        elementsCollectionEclipse.add(3);
    }

    @Benchmark
    public void testKolobokeAdd(){
        elementsKoloboke.add(1,1);
        elementsKoloboke.add(3,3);
    }

    @Benchmark
    public void testTreeSetAdd(){
        elementsTreeSet.add(1);
        elementsTreeSet.add(3);
    }

    elementsHashSet.add(1);
    elementsArrayList = new ArrayListImpl<>();
    elementsConcurrentHash = new ConcurrentHashMapImpl<>();
    elementsCollectionEclipse = new EclipseCollection<>();
    elementsKoloboke = new Koloboke();
    elementsTreeSet.add(1);

    @Benchmark
    public void testTreeSetRemove(){
        elementsTreeSet.add(1);
        elementsTreeSet.add(3);
    }
    @Benchmark
    public void testKolobeRemove(){
        elementsTreeSet.add(1);
        elementsTreeSet.add(3);
    }
}
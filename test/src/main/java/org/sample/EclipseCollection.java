package org.sample;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;

public class EclipseCollection<T> {
    MutableList<T> elements;

    public EclipseCollection(){
        elements = Lists.mutable.empty();
     }

     public boolean add(T elem){
        return elements.add(elem);
     }

     public boolean contains(T elem){
        return elements.contains(elem);
     }

     public boolean remove(T elem){
        return elements.remove(elem);
     }
}

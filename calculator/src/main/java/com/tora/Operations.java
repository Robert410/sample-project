package com.tora;

import java.util.Objects;

public class Operations {
    public double number1;
    public double number2;
    public String operator;

    public Operations(String op, double nr1, double nr2){
        operator = op;
        number1 = nr1;
        number2 = nr2;
    }

    public double calculate(){
        double ans = 0;
        switch(operator) {
            case "+": ans = number1 + number2;
                break;
            case "-": ans = number1 - number2;
                break;
            case "*": ans = number1 * number2;
                break;
            case "/": ans = number1 / number2;
                break;
            case "min": ans = Math.min(number1, number2);
                break;
            case "max": ans = Math.max(number1, number2);
                break;
            case "sqrt": ans = Math.sqrt(number1);
                break;
            default: System.out.println("no operator found");
                break;
        }
        return ans;
    }

    public String showResult(){
        double ans = calculate();
        if(Objects.equals(operator, "sqrt"))
            return "sqrt(" + String.valueOf(number1) + ") = " + String.valueOf(ans);
        if(Objects.equals(operator, "min") || Objects.equals(operator, "max")){
            return operator + "(" + number1 + ", " + number2 + ") = " + ans;
        }
        return String.valueOf(number1) + " " + operator + " " + String.valueOf(number2) + " = " + String.valueOf(ans);
    }

}

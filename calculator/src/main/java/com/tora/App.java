package com.tora;

import java.util.Objects;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class App {
    public static void main(String[] args) {
        String lines;
        Scanner reader = new Scanner(System.in);
        lines = reader.nextLine();
        while (!Objects.equals(lines, "stop")) {
            String[] parts = lines.split(" ");
            if (Objects.equals(parts[0], "sqrt")) {
                Operations op = new Operations(parts[0], parseInt(parts[1]), 0);
                System.out.println(op.showResult());
            } else if (Objects.equals(parts[0], "min") || Objects.equals(parts[0], "max")) {
                Operations op = new Operations(parts[0], parseInt(parts[1]), parseInt(parts[2]));
                System.out.println(op.showResult());
            } else {
                Operations op = new Operations(parts[1], parseInt(parts[0]), parseInt(parts[2]));
                System.out.println(op.showResult());
            }

            lines = reader.nextLine();
        }
    }
}

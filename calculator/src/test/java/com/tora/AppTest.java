package com.tora;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AppTest {
  @Test
  public void testSqrt(){
    Operations op7 = new Operations("sqrt", 1, 2);
    assertEquals(1.0, op7.calculate());
    assertEquals("sqrt(1.0) = 1.0", op7.showResult());

  }
  @Test
  public void testMax(){
    Operations op6 = new Operations("max", 1, 2);
    assertEquals(2.0, op6.calculate());
    assertEquals("max(1.0, 2.0) = 2.0", op6.showResult());

  }
  @Test
  public void testMin(){
    Operations op5 = new Operations("min", 1, 2);
    assertEquals(1.0, op5.calculate());
    assertEquals("min(1.0, 2.0) = 1.0", op5.showResult());

  }
  @Test
  public void testDivision(){
    Operations op4 = new Operations("/", 1, 2);
    assertEquals(0.5, op4.calculate());
    assertEquals("1.0 / 2.0 = 0.5", op4.showResult());

  }
  @Test
  public void testMultiply(){
    Operations op3 = new Operations("*", 2, 4);
    assertEquals(8.0, op3.calculate());
    assertEquals("2.0 * 4.0 = 8.0", op3.showResult());

  }
  @Test
  public void testSubstract(){
    Operations op2 = new Operations("-", 1, 2);
    assertEquals(-1.0, op2.calculate());
    assertEquals("1.0 - 2.0 = -1.0", op2.showResult());

  }
  @Test
  public void testAdd(){
    Operations op1 = new Operations("+", 1, 2);
    assertEquals(3.0, op1.calculate());
    assertEquals("1.0 + 2.0 = 3.0", op1.showResult());

  }



}

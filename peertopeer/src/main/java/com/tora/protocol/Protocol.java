package com.tora.protocol;

public enum Protocol {
    INIT("!hello "),

    ACK("!ack"),
    DONE_CHAT("!disc");

    private final String header;

    Protocol(final String header) {
        this.header = header;
    }

    public String header() {
        return header;
    }

    public static String composeMessage(final Protocol protocol, final String body) {
        return protocol.header + " " + body;
    }
}

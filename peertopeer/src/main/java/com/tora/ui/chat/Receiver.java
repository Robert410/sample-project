package com.tora.ui.chat;

import java.io.BufferedReader;
import java.io.IOException;

public final class Receiver implements Runnable {

    private final BufferedReader reader;
    private final SynchronizationCondition condition;

    public Receiver(final BufferedReader reader, final SynchronizationCondition condition) {
        this.reader = reader;
        this.condition = condition;
    }

    @Override
    public void run() {
        while (!condition.done()) {
            try {
                System.out.println(reader.readLine());
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }
}
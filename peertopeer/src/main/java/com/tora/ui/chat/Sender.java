package com.tora.ui.chat;

import com.tora.protocol.Protocol;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Scanner;

public final class Sender implements Runnable {

    private final Scanner scanner;
    private final BufferedWriter writer;
    private final SynchronizationCondition condition;

    public Sender(final Scanner scanner, final BufferedWriter writer, final SynchronizationCondition condition) {
        this.scanner = scanner;
        this.writer = writer;
        this.condition = condition;
    }

    @Override
    public void run() {
        while (!condition.done()) {
            final String line = scanner.nextLine();

            if (Protocol.DONE_CHAT.header().equals(line)) {
                condition.done(true);
                condition.signal();
            } else {
                try {
                    writer.write(line + "\n");
                    writer.flush();
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
            }
        }
    }
}

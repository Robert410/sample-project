package com.tora.ui.chat;

import java.util.concurrent.atomic.AtomicBoolean;

public final class SynchronizationCondition {

    private final AtomicBoolean done;
    private final Object mutex;

    public SynchronizationCondition(final boolean done) {
        this.done = new AtomicBoolean(done);
        this.mutex = new Object();
    }

    public boolean done() {
        return done.get();
    }

    public void done(final boolean done) {
        this.done.set(done);
    }

    public void await() throws InterruptedException {
        synchronized (mutex) {
            mutex.wait();
        }
    }

    public void signal() {
        synchronized (mutex) {
            mutex.notify();
        }
    }
}